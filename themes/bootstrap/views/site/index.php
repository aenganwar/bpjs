<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<?php
            $baseUrl = Yii::app()->theme->baseUrl; 
            $cs = Yii::app()->getClientScript();
            Yii::app()->clientScript->registerCoreScript('jquery');
        ?>


<div class="row" id="searching-panel">
   <div class="span12">
      <div id="searching-panel">
	
      <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		      'id'=>'searching-form',
		      'enableAjaxValidation'=>false,
		      'action'=>array('site/index'),
	      )); ?>
	      
	      <div class="control-group" style="margin-bottom:0;">
			<!--<label class="control-label required" for="Produk_kategori_produk_id"> Kata Pencari</label>-->
			<input type="submit" name="search" value ="search" class="btn btn-success" style="float: right; margin-top:0px; margin-left:5px">
			<select id="kabupaten_id" name="kabupaten" class="span3 chzn-select" style="float: right">
				<option value="0" selected="selected">- Pilih Kabupaten/Kota -</option>
				<?php foreach(Kabupaten::model()->findAll(array('order'=>'nama')) as $kabupaten){?>
					
					  <?php foreach(Kabupaten::model()->findAllByAttributes(array('id'=>$kabupaten->id)) as $kelurahan){?>
					
					  <?php foreach(Kabupaten::model()->findAllByAttributes(array('id'=>$kelurahan->id), array('order'=>'nama')) as $kabupaten){?>
					     <option value="<?php echo $kabupaten->id?>"><?php echo $kabupaten->nama?></option>
				       <?php }?>
				    <?php }?>
				<?php }?>
			</select>

			<input type="text" name="keyword" class="span3" placeholder="Kata Pencari..." style="float: right">

		</div>
	      
      <?php $this->endWidget(); ?>
      <?php $this->widget( 'ext.EChosen.EChosen' ); ?>
      </div>
   </div>
</div>
 <div class="brand" href="#"><img src="<?php echo $baseUrl;?>/images/logo1.png" style="margin-top: -110px !important; margin-left:65px; margin-bottom:30px"></div>
<div class="row-fluid">
   <div id="nav-left" class="span2">
      <h5 class="caption">Fasilitas Kesehatan</h5>
      <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'select-form',
		'enableAjaxValidation'=>false,
		'action'=>array('site/select'),
	)); ?>
      <table width="100%">
	<?php foreach(FaskesTipe::model()->findAll(array('order'=>'kode')) as $faskesTipe){?>
		<tr>
			<td><input style="margin-bottom: 7px;" type="checkbox" id="selectBox[]" name="selectBox[]"class="selectBox" value="<?php echo $faskesTipe->id;?>"></td>
			<td style="padding:3px 5px; "><img style="height: 25px" id="icon-mark" src="<?php echo Yii::app()->request->baseUrl.'/images/map_icons/'.$faskesTipe->icon;?>"></td>
			<td style="padding:3px 0"><?php echo $faskesTipe->kode.' - '.$faskesTipe->jenis;?></td>
		</tr>
	<?php }?>
	</table>
      <?php $this->endWidget(); ?>
   </div>
   
   <div id="content-map" class="span10">
	 <div id="map">
	    <?php 
	    //
	    // ext is your protected.extensions folder
	    // gmaps means the subfolder name under your protected.extensions folder
	    //  
	    Yii::import('ext.gmap.*');
	     
	    $gMap = new EGMap();
	    $gMap->zoom = 5;
	    $mapTypeControlOptions = array(
	      'position'=> EGMapControlPosition::LEFT_BOTTOM,
	      'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU
	    );
	     
	    $gMap->mapTypeControlOptions= $mapTypeControlOptions;
	     
	    $gMap->setCenter(-1.419234, 117.475601);
	     
	    foreach($model as $faskes):
	    $objek = '<div id="showData-map">
			    <h4 class="judul">'.CHtml::link($faskes->nama, array('/site/viewFaskes', 'id'=>$faskes->id)).'</h4>
			    <p class="content-kategori">'.FaskesTipe::model()->findByPk($faskes->faskes_tipe_id)->jenis.' ('.FaskesTipe::model()->findByPk($faskes->faskes_tipe_id)->kode.')</p>
			      <div class="row-fluid">
				   <div class="span4">';
				   if(FaskesImg::model()->findAllByAttributes(array('faskes_id'=>$faskes->id)) !=null)
				    {
					    foreach(FaskesImg::model()->findAllByAttributes(array('faskes_id'=>$faskes->id)) as $key=>$gambar)
					    {
						$objek.='<img id="thumb-map" src="'.Yii::app()->baseUrl.'/images/faskes/'.$gambar->img.'">';   
						break;
					     }
				    }else{
					     $objek.='<img id="thumb-map" src="'.Yii::app()->baseUrl.'/images/faskes/no-img.png" />';
				    }
				  $objek.=' </div>
				   <div class="span8">
				       <p class="content-objek">'.substr(strip_tags($faskes->keterangan),0,100).' ...</p>
				   </div>
			       </div>
		     </div>
			';
	    // Create GMapInfoWindows
	    $info_window = new EGMapInfoWindow($objek);
	    $icon = new EGMapMarkerImage(Yii::app()->request->baseUrl.'/images/map_icons/'.FaskesTipe::model()->findByPk($faskes->faskes_tipe_id)->icon);
	    $icon->setSize(40, 40);
	    $icon->setAnchor(16, 16.5);
	    $icon->setOrigin(0, 0);
	    
	    
	    // Create marker
	    $marker = new EGMapMarker($faskes->lat, $faskes->long, array('title' => $faskes->nama, 'icon'=>$icon));
	    $marker->addHtmlInfoWindow($info_window);
	    $gMap->addMarker($marker);
	    
	    // enabling marker clusterer just for fun
	    // to view it zoom-out the map
	    $gMap->enableMarkerClusterer(new EGMapMarkerClusterer());
	    endforeach;
	     
	    $gMap->renderMap();
	    ?>
	    </div>
   </div>
</div>

<script>
$(document).ready(function(){
  $(".selectBox").click(function(){
    //alert($(this).attr('id'));
	setTimeout(function() {
	   $('form#select-form').submit();
	}, 1000);
  });
});
</script>