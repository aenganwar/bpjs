<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;

?>

<h2 style="text-align: center"><?php echo $model->nama;?></h2>
<hr>
<div class="row" style="margin-left: 30px" >
    <div class="span3">
        <h5 class="caption">Data Fasilitas Kesehatan</h5>
        <p>Nama Fasilitas Kesehatan : <?php echo $model->nama;?></p>
        <p>Tipe Fasilitas Kesehatan : <?php echo FaskesTipe::model()->findByPk($model->faskes_tipe_id)->kode.' - '.FaskesTipe::model()->findByPk($model->faskes_tipe_id)->jenis;?></p>
        <p>Branch : <?php echo Branch::model()->findByPk($model->branch_id)->branch;?></p>
        <p>Class Fasilitas : <?php echo FaskesClass::model()->findByPk($model->faskes_class_id)->class;?></p>
        <p>Alamat : <?php echo $model->alamat;?></p>
        <p>Kota/Kabupaten : <?php echo Kabupaten::model()->findByPk(Kecamatan::model()->findByPk(Kelurahan::model()->findByPk($model->kelurahan_id)->kecamatan_id)->kabupaten_id)->nama;?></p>
        <p>Telephone : <?php echo $model->phone;?></p>
        <p>Fax : <?php echo $model->fax;?></p>
        <p>Hotline : <?php echo $model->hotline;?></p>
        <br>
        <h5 class="caption">Data Tenaga Medis</h5>
        <table width=100%>
            <tr>
                <th>No</th>
                <th>Jenis Tenaga Medis</th>
                <th>Jumlah</th>
            </tr>
        <?php
        $i=1;
        foreach(JenisTenagaMedis::model()->findAll() as $jenisTenaga){?>
            <tr>
                <td><?php echo $i++;?></td>
                <td><?php echo $jenisTenaga->jenis;?></td>
                <td><?php echo count(TenagaMedis::model()->findAllByAttributes(array('jenis_tenaga_medis_id'=>$jenisTenaga->id, 'faskes_id'=>$model->id)));?></td>
            </tr>
        <?php }?>
        </table>
    </div>
    
    <div class="span6">
        <h5 class="caption">Deskripsi</h5>
        <p><?php echo $model->keterangan;?></p>
    </div>
    
    <div class="span3">
        <h5 class="caption">Gambar Fasilitas Kesehatan</h5>
         <?php if(FaskesImg::model()->findAllByAttributes(array('faskes_id'=>$model->id)) != null){?>
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              <li data-target="#carousel-example-generic" data-slide-to="2"></li>
            </ol>
         
          <?php foreach(FaskesImg::model()->findAllByAttributes(array('faskes_id'=>$model->id)) as $gambar){?>
            <!-- Wrapper for slides -->
            <div class="carousel-inner">
              <div class="item active">
                <img src="<?php echo Yii::app()->baseUrl;?>/images/faskes/<?php echo $gambar->img;?>" alt="<?php echo $model->nama;?>">
                <div class="carousel-caption">
                  <?php echo $gambar->keterangan;?>
                </div>
              </div>
            <?php }?>
            </div>
          
            <!-- Controls -->
            <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left"></span>
            </a>
            <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right"></span>
            </a>
          </div>
        <?php }else{?>
            <center><h4>Tidak Ada Gambar</h4></center>
        <?php }?>
        <h5 class="caption">Index Pelayanan</h5>
        <img width="30px" src="<?php echo Yii::app()->baseUrl;?>/images/love.png"> <img width="30px" src="<?php echo Yii::app()->baseUrl;?>/images/love.png"> <img width="30px" src="<?php echo Yii::app()->baseUrl;?>/images/love.png">
    </div>
</div>
