<h1>Tandai Peta</h1>

<div id="myMap" style="height:450px; width:100%;"></div>

<!--input type="button" value="Clear map" onclick="BlitzMap.deleteAll();" /-->
<input type="button" value="Toggle edit mode" onclick="BlitzMap.toggleEditable();" />
<input type="button" value="Generate Map Text" onclick="BlitzMap.toJSONString()" />
<!--input type="button" value="Create Map from JSON" onclick="BlitzMap.setMap( 'myMap', true, 'mapData' ); BlitzMap.init()" /-->
<input type="button" onclick="javascript:changeParent()" value="Close">
<?php 
	
	$json_string = '{"zoom":5,"tilt":0,"mapTypeId":"hybrid","center":{"lat":-0.729824, "lng":117.434456},"overlays":[]}';
	
	$faskes = Faskes::model()->findByPk($faskes_id);
	if(!is_null($faskes))
	{
		$json_string = $faskes->map_text;
	}
?>
<textarea id="mapData" style="width:100%; height:200px"><?php echo $json_string; ?></textarea>
<script type="text/javascript">
	BlitzMap.setMap( 'myMap', true, 'mapData' );
</script>
<script language="javascript"> 
function changeParent() { 
  BlitzMap.toJSONString();
  window.opener.document.getElementById('Faskes_map_text').value=$("#mapData").val();
  var obj = jQuery.parseJSON( $("#mapData").val() );
  window.opener.document.getElementById('Faskes_lat').value=obj.center.lat;
  window.opener.document.getElementById('Faskes_long').value=obj.center.lng;
  window.close();
} 
</script> 

