<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<?php /*
<div class="row" id="searching-panel">
   <div class="span12">
      <h5>Searching</h5>
      <div id="searching-panel">
      <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		      'id'=>'searching-form',
		      'enableAjaxValidation'=>false,
		      'action'=>array('site/index'),
	      )); ?>
	      
	      <div class="control-group" style="margin-bottom:0">
			<!--<label class="control-label required" for="Produk_kategori_produk_id"> Kata Pencari</label>-->
			<input type="text" name="keyword" class="span3" placeholder="Kata Pencari...">
			
			<select id="kabupaten_id" name="kabupaten" class="span3">
				<option value="0" selected="selected">- Pilih Kabupaten/Kota -</option>
				<?php foreach(Kabupaten::model()->findAll(array('order'=>'nama')) as $kabupaten){?>
					
					  <?php foreach(Kabupaten::model()->findAllByAttributes(array('id'=>$kabupaten->id)) as $kelurahan){?>
					
					  <?php foreach(Kabupaten::model()->findAllByAttributes(array('id'=>$kelurahan->id), array('order'=>'nama')) as $kabupaten){?>
					     <option value="<?php echo $kabupaten->id?>"><?php echo $kabupaten->nama?></option>
				       <?php }?>
				    <?php }?>
				<?php }?>
			</select>
		</div>
	      
      <?php $this->endWidget(); ?>
      </div>
   </div>
</div>
*/?>
 <?php
            $baseUrl = Yii::app()->theme->baseUrl; 
            $cs = Yii::app()->getClientScript();
            Yii::app()->clientScript->registerCoreScript('jquery');
        ?>
<hr>
<div class="row-fluid">
   <div id="nav-left" class="span2">
      <h5 class="caption">Data Kota</h5>
      <table width="100%">
	<?php foreach(Kabupaten::model()->findAll(array('order'=>'id')) as $namaKota){?>
		<tr>
			<td><input style="margin-bottom: 7px;" type="checkbox" id="selectBox[]" name="selectBox[]"class="selectBox" value="<?php echo $namaKota->id;?>"></td>
			<td style="padding:3px 0"><?php echo $namaKota->nama;?></td>
		</tr>
	<?php }?>
	</table>
   </div>
   
   <div id="content" class="span10">
		<center><img src="<?php echo $baseUrl ;?>/images/alamat.png"></center>
	</div>
