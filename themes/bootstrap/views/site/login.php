<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);

?>
<div class="panel-login">
	<div class="panel-login-header">
		Login Form
	</div>	
	<div class="panel-login-boddy">
	<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>
		<?php echo $form->textFieldRow($model,'username', array('class'=>'span3','prepend'=>'<i class="icon-user"></i>', 'placeholder'=>'username...', 'label'=>false)); ?>
		<br>
			
		<?php echo $form->passwordFieldRow($model,'password', array(
								'class'=>'span3',
								//'id'=>'password',
								'prepend'=>'<i class="icon-lock"></i>',
								'placeholder'=>'password...',
								'label'=>false));
		?>
		
		<?php //echo '<img style="margin-right:10px; margin-top:-10px" id="passwd" class="pull-right" rel="tooltip"
		//title="Click to open the virtual keyboard" width="20px" src="'.Yii::app()->request->baseUrl.'/images/keyboard2.png">' ?>
		
	<div style="text-align: left">
		<?php echo $form->checkBoxRow($model,'rememberMe', array(
									 'class'=>'span1',
									 'style'=>'text-align:left',
									 )); ?>	
	</div>
		<?php
		$this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
			'label'=>'Login',
			//'icon'=>'lock',
			'htmlOptions'=>array(
					     'id'=>'btnLogin',
					     ),
		    ));
		?>
		
		
	<?php $this->endWidget(); ?>
	</div>
</div>
