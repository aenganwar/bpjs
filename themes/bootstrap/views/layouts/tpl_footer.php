<?php
            $baseUrl = Yii::app()->theme->baseUrl; 
            $cs = Yii::app()->getClientScript();
            Yii::app()->clientScript->registerCoreScript('jquery');
        ?>
<!-- Footer -->
        <footer>
            <div class="container">
                <div class="footer-border"></div>
                <div class="row">
                    <div class="copyright span4">
                        <p>Copyright 2014 Digitak - All rights reserved. <?php echo CHtml::link('Administrator', array('site/administrator'));?>.</p>
                    </div>
                    <div class="social span8">
                        <a class="facebook" href=""></a>
                        <a class="dribbble" href=""></a>
                        <a class="twitter" href=""></a>
                        <a class="pinterest" href=""></a>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="<?php echo $baseUrl;?>/js/jquery-1.8.2.min.js"></script>
        <script src="<?php echo $baseUrl;?>/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo $baseUrl;?>/js/jquery.flexslider.js"></script>
        <script src="<?php echo $baseUrl;?>/js/jquery.tweet.js"></script>
        <script src="<?php echo $baseUrl;?>/js/jflickrfeed.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="<?php echo $baseUrl;?>/js/jquery.ui.map.min.js"></script>
        <script src="<?php echo $baseUrl;?>/js/jquery.quicksand.js"></script>
        <script src="<?php echo $baseUrl;?>/prettyPhoto/js/jquery.prettyPhoto.js"></script>
        <script src="<?php echo $baseUrl;?>/js/scripts.js"></script>

    </body>

</html>

