<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles_admin.css" />
	<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl; ?>/images/favico.ico" type="image/x-icon" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

	<?php //Yii::app()->bootstrap->register(); ?>
		
<script language="JavaScript" type="text/JavaScript">
	function timer()
	{
		var d = new Date();  
		var jam = d.getHours();
		var menit = d.getMinutes();
		var detik = d.getSeconds();
		var strwaktu = (jam<10)?"0"+jam:jam;
		
		strwaktu +=(menit<10)?":0"+menit:":"+menit;
		strwaktu +=(detik<10)?":0"+detik:":"+detik;
		$(".timer").html(strwaktu);
		setTimeout("timer()",200);
		
		Number.prototype.formatMoney = function(c, d, t){
		var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "," : d, t = t == undefined ? "." : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
		return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
		};
	}
	
</script>
	
</head>

<body class="view-login" onload="timer()">
	
	
	<div class="container">
		<?php echo $content; ?>
	</div>
	
	<div class="time-login">
		<?php echo '<img src="'.Yii::app()->request->baseUrl.'/images/time.png" style="margin-bottom:5px">'?>
		<?php echo date("d/m/Y")?>, <span class="timer"></span> <?php echo date("A");?>
	</div>
</body>
</html>