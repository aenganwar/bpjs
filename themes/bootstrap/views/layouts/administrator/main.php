<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
	
	<script type='text/javascript'>
	$(document).ready(function () {
		if ($("[rel=tooltip]").length) {
		$("[rel=tooltip]").tooltip();
		}
	});
	</script>
	<?php
	  $baseUrl = Yii::app()->theme->baseUrl; 
	  $cs = Yii::app()->getClientScript();
	  Yii::app()->clientScript->registerCoreScript('jquery');
	?>
	
    <!-- the styles -->
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $baseUrl;?>/css/font-awesome/css/font-awesome.min.css">
    <link rel="shortcut icon" href="<?php echo $baseUrl;?>/img/ico/favicon.ico">
    
    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
	
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles_admin.css" />
	
</head>

<body>
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
		<div class="container-fluid">
		<div style="color:#fff; margin-top:12px; font-size: 12px" class="pull-right">
		<div style="float: left; margin-right: 10px; margin-top: -10px">
			<a href="<?php echo Yii::app()->controller->createUrl('site/index');?>" target="_blank" class="btn btn-success btn-small"><i class="icon-eye-open icon-white"></i> Halaman Depan</a>
		</div>
			Hallo, <span style="color:#DD4B39; font-weight:bold"><?php echo Yii::app()->user->id != null ? User::model()->findByPk(Yii::app()->user->id)->username : '-';?></span> | <a href="<?php echo Yii::app()->controller->createUrl('/site/logout');?>"><b>Logout</b></a>	
		</div>
		<a href="#" class="brand"><span class="first">Fasilitas BPJS</span></a>
		</div>
	</div>
</div>
<div id="page" class="container-fluid">
	<div class="row-fluid">
		<div id="left"  class="span2">
			<legend><h2><strong>Main Menu</strong></h2></legend>
			
			<div id="accordian">
				<ul>
					<li><a href="<?php echo Yii::app()->controller->createUrl('/site/administrator');?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
					
					
					<li>
						<h3><i class="fa fa-archive"></i> Data BPJS</h3>
						<ul>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/divre/admin');?>"><i class="fa fa-check-square"></i> Divisi Regional</a></li>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/branch/admin');?>"><i class="fa fa-check-square"></i> Branch</a></li>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/Faskes/admin');?>"><i class="fa fa-check-square"></i> Fasilitas Kesehatan</a></li>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/FaskesTipe/admin');?>"><i class="fa fa-check-square"></i> Tipe Fasilitas</a></li>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/jenisTenagaMedis/admin');?>"><i class="fa fa-check-square"></i> Jenis Tenaga Medis</a></li>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/tenagaMedis/admin');?>"><i class="fa fa-check-square"></i> Tenaga Medis</a></li>	
						</ul>
					</li>
					
					<li>
						<h3><i class="fa fa-archive"></i> Data Master</h3>
						<ul>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/provinsi/admin');?>"><i class="fa fa-check-square"></i> Provinsi</a></li>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/kabupaten/admin');?>"><i class="fa fa-check-square"></i> Kabupaten/Kota</a></li>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/kecamatan/admin');?>"><i class="fa fa-check-square"></i> Kecamatan</a></li>
							<li><a href="<?php echo Yii::app()->controller->createUrl('/kelurahan/admin');?>"><i class="fa fa-check-square"></i> Kelurahan/Desa</a></li>
							</ul>
					</li>
					
					<li>
						<h3><i class="fa fa-users"></i> User Akses</h3>
						<ul>
							<!--<li><a href="<?php //echo Yii::app()->controller->createUrl('/role/admin');?>"><i class="fa fa-check-square"></i> User Role</a></li>-->
							<li><a href="<?php echo Yii::app()->controller->createUrl('/user/admin');?>"><i class="fa fa-check-square"></i> User</a></li>
						</ul>
					</li>
				    <li><a href="<?php echo Yii::app()->controller->createUrl('/site/logout');?>"><i class="fa fa-power-off"></i> Logout</a></li>
				</ul>
			</div>
		</div>
		
		<!-- Javascript ChartJS-->
		<script language="JavaScript" type="text/JavaScript">
			$(document).ready(function(){
				$("#accordian h3").click(function(){
					//slide up all the link lists
					$("#accordian ul ul").slideUp();
					//slide down the link list below the h3 clicked - only if its closed
					if(!$(this).next().is(":visible"))
					{
						$(this).next().slideDown();
					}
				})
			})
		</script>
		<div id="right" class="span10">
			<?php echo $content; ?>
		</div>
	</div>
	<!--Footer Panel -->
	<div class="clear"></div>
	<div id="footer">	
		Copyright &copy <?php echo date('Y');?> Digital Data Studio
	</div><!-- footer -->
</div><!--page-->

</body>
</html>
