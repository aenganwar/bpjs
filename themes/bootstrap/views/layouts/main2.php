<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	<?php //Yii::app()->bootstrap->register(); ?>
	
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
	'type'=>'', // null or 'inverse'
    'brandUrl'=>'#',
    'collapse'=>true, // requires bootstrap-responsive.css
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
                array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
		array('label'=>'Cagar Budaya', 'url'=>'#', 'items'=>array(
                    array('label'=>'Bangunan', 'url'=>'#'),
                    array('label'=>'Tugu/Monumen', 'url'=>'#'),
                    array('label'=>'Museum', 'url'=>'#'),
                    '---',
                    array('label'=>'Data Lainnya'),
                    array('label'=>'Kawasan Cagar Budaya', 'url'=>'#'),
                    array('label'=>'Tempat Bersejarah', 'url'=>'#'),
                )),
                
		array('label'=>'Master Data', 'url'=>'#', 'items'=>array(
		    array('label'=>'Bangunan', 'url'=>array('/bangunan/admin')),
                    '---',
		    array('label'=>'Kecamatan', 'url'=>array('/kecamatan/admin')),
                    array('label'=>'Kelurahan', 'url'=>array('/kelurahan/admin'), ),
                ), 'visible'=>!Yii::app()->user->isGuest),
		
		array('label'=>'User Role', 'url'=>'#', 'items'=>array(
		    array('label'=>'User', 'url'=>array('/user/admin')),
                    array('label'=>'Role', 'url'=>array('/role/admin'), ),
                ), 'visible'=>!Yii::app()->user->isGuest),
		
		//array('label'=>'Contact', 'url'=>array('/site/contact')),
            ),
        ),
		 '<form class="navbar-search pull-right" action=""><input type="text" class="search-query span2" placeholder="Search"></form>',
		
		array(
			'class'=>'bootstrap.widgets.TbMenu',
			'htmlOptions'=>array('class'=>'pull-right'),
			'items'=>array(
			    array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
			    array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		),
		
    ),
)); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		<hr>
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
