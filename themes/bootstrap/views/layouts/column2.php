<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="span9">
        <div id="content">
            
            <?php
                    foreach(Yii::app()->user->getFlashes() as $key => $message) {
                            echo '<div class="alert alert-' . $key . '">';
                            echo '<button type="button" class="close" data-dismiss="alert">�</button>';
                            print $message;
                            print "</div>\n";
                            
                            
                    }
            ?>
            
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
    <div class="span3">
        <div id="sidebar">
        <?php
            $this->beginWidget('zii.widgets.CPortlet', array(
                'title'=>'Operations',
            ));
            $this->widget('bootstrap.widgets.TbMenu', array(
                'items'=>$this->menu,
                'htmlOptions'=>array('class'=>'operations'),
            ));
            $this->endWidget();
        ?>
        </div><!-- sidebar -->
    </div>
</div>
<?php $this->endContent(); ?>