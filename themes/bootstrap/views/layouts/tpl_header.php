<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <title>WebGis Fasilitas Kesehatan BPJS</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <?php
            $baseUrl = Yii::app()->theme->baseUrl; 
            $cs = Yii::app()->getClientScript();
            Yii::app()->clientScript->registerCoreScript('jquery');
        ?>
        <!-- CSS -->
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,400">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Sans">
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Lobster">
        <link rel="stylesheet" href="<?php echo $baseUrl;?>/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo $baseUrl;?>/prettyPhoto/css/prettyPhoto.css">
        <link rel="stylesheet" href="<?php echo $baseUrl;?>/css/flexslider.css">
        <link rel="stylesheet" href="<?php echo $baseUrl;?>/css/font-awesome.css">
        <link rel="stylesheet" href="<?php echo $baseUrl;?>/css/style.css">

        <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
            <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="<?php echo $baseUrl;?>/ico/favicon.ico">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="<?php echo $baseUrl;?>/ico/apple-touch-icon-144-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="<?php echo $baseUrl;?>/ico/apple-touch-icon-114-precomposed.png">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="<?php echo $baseUrl;?>/ico/apple-touch-icon-72-precomposed.png">
        <link rel="apple-touch-icon-precomposed" href="<?php echo $baseUrl;?>/ico/apple-touch-icon-57-precomposed.png">

    </head>

    <body>

        <!-- Header -->