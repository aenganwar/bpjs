<?php
            $baseUrl = Yii::app()->theme->baseUrl; 
            $cs = Yii::app()->getClientScript();
            Yii::app()->clientScript->registerCoreScript('jquery');
        ?>
<div class="container">
            <div class="header row">
                <div class="span12">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </a>
                            <div class="nav-collapse collapse">
                            
                                <ul class="nav pull-right">
                                    <li class="current-page">
                                        <?php echo CHtml::link('Home', array('/site/index'));?>
                                    </li>
                                    <li>
                                        <?php echo CHtml::link('Data', array('/site/index'));?>
                                    </li>
                                    <li>
                                        <?php echo CHtml::link('Profil', array('/site/index'));?>
                                    </li>
                                    </li>
                                    <li>
                                        <?php echo CHtml::link('Contact', array('/site/contact'));?>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
