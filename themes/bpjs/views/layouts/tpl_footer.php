<?php
            $baseUrl = Yii::app()->theme->baseUrl; 
            $cs = Yii::app()->getClientScript();
            Yii::app()->clientScript->registerCoreScript('jquery');
        ?>
<!-- Footer -->
        <footer>
            <div class="container">
                <div class="row">
                    <div class="widget span3">
                        <h4>About Us</h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et.</p>
                        <p><a href="">Read more...</a></p>
                    </div>
                    <div class="widget span3">
                        <h4>Latest Tweets</h4>
                        <div class="show-tweets"></div>
                    </div>
                    <div class="widget span3">
                        <h4>Flickr Photos</h4>
                        <ul class="flickr-feed"></ul>
                    </div>
                    <div class="widget span3">
                        <h4>Contact Us</h4>
                        <p><i class="icon-map-marker"></i> Address: Via Principe Amedeo 9, 10100, Torino, TO, Italy</p>
                        <p><i class="icon-phone"></i> Phone: 0039 333 12 68 347</p>
                        <p><i class="icon-user"></i> Skype: Andia_Agency</p>
                        <p><i class="icon-envelope-alt"></i> Email: <a href="">contact@andia.co.uk</a></p>
                    </div>
                </div>
                <div class="footer-border"></div>
                <div class="row">
                    <div class="copyright span4">
                        <p>Copyright 2012 Andia - All rights reserved. Template by <a href="http://azmind.com">Azmind</a>.</p>
                    </div>
                    <div class="social span8">
                        <a class="facebook" href=""></a>
                        <a class="dribbble" href=""></a>
                        <a class="twitter" href=""></a>
                        <a class="pinterest" href=""></a>
                    </div>
                </div>
            </div>
        </footer>

        <!-- Javascript -->
        <script src="<?php echo $baseUrl;?>/js/jquery-1.8.2.min.js"></script>
        <script src="<?php echo $baseUrl;?>/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo $baseUrl;?>/js/jquery.flexslider.js"></script>
        <script src="<?php echo $baseUrl;?>/js/jquery.tweet.js"></script>
        <script src="<?php echo $baseUrl;?>/js/jflickrfeed.js"></script>
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script>
        <script src="<?php echo $baseUrl;?>/js/jquery.ui.map.min.js"></script>
        <script src="<?php echo $baseUrl;?>/js/jquery.quicksand.js"></script>
        <script src="<?php echo $baseUrl;?>/prettyPhoto/js/jquery.prettyPhoto.js"></script>
        <script src="<?php echo $baseUrl;?>/js/scripts.js"></script>

    </body>

</html>

