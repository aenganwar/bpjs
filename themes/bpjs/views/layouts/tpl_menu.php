<div class="container">
            <div class="header row">
                <div class="span12">
                    <div class="navbar">
                        <div class="navbar-inner">
                            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </a>
                            <h1>
                                <a class="brand" href="index.html">BPJS - Untuk pelayanan yang lebih baik..</a>
                            </h1>
                            <div class="nav-collapse collapse">
                                <ul class="nav pull-right">
                                    <li class="current-page">
                                        <a href="index.html"><i class="icon-home"></i><br />Home</a>
                                    </li>
                                    <li>
                                        <a href="portfolio.html"><i class="icon-camera"></i><br />Pencarian</a>
                                    </li>
                                    <li>
                                        <a href="#"><i class="icon-comments"></i><br />Fasilitas Kesehatan</a>
                                    </li>
                                    <li>
                                        <a href="services.html"><i class="icon-tasks"></i><br />BPJS</a>
                                    </li>
                                    <li>
                                        <a href="about.html"><i class="icon-user"></i><br />About</a>
                                    </li>
                                    <li>
                                        <a href="contact.html"><i class="icon-envelope-alt"></i><br />Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
