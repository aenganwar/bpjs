<?php

/**
 * This is the model class for table "tenaga_medis".
 *
 * The followings are the available columns in table 'tenaga_medis':
 * @property integer $id
 * @property string $nama
 * @property integer $jenis_tenaga_medis_id
 * @property string $alamat
 * @property integer $kabupaten_id
 */
class TenagaMedis extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tenaga_medis';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nama, jenis_tenaga_medis_id', 'required'),
			array('jenis_tenaga_medis_id, faskes_id, kabupaten_id', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>255),
			array('alamat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nama, jenis_tenaga_medis_id, faskes_id, alamat, kabupaten_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nama' => 'Nama',
			'jenis_tenaga_medis_id' => 'Jenis Tenaga Medis',
			'faskes_id' => 'Fasilitas Kesehatan',
			'alamat' => 'Alamat',
			'kabupaten_id' => 'Kabupaten',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('jenis_tenaga_medis_id',$this->jenis_tenaga_medis_id);
		$criteria->compare('faskes_id',$this->faskes_id);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TenagaMedis the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
