<?php

/**
 * This is the model class for table "branch".
 *
 * The followings are the available columns in table 'branch':
 * @property integer $id
 * @property string $branch
 * @property integer $divre_id
 * @property string $alamat
 * @property integer $kabupaten_id
 * @property string $zip
 * @property string $phone
 * @property string $fax
 * @property string $hotline
 * @property string $lat
 * @property string $long
 */
class Branch extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'branch';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('branch, divre_id', 'required'),
			array('divre_id, kabupaten_id', 'numerical', 'integerOnly'=>true),
			array('branch, lat, long', 'length', 'max'=>255),
			array('zip, phone, fax, hotline', 'length', 'max'=>50),
			array('alamat', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, branch, divre_id, alamat, kabupaten_id, zip, phone, fax, hotline, lat, long', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branch' => 'Branch',
			'divre_id' => 'Regional',
			'alamat' => 'Alamat',
			'kabupaten_id' => 'Kabupaten',
			'zip' => 'Zip',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'hotline' => 'Hotline',
			'lat' => 'Lat',
			'long' => 'Long',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('branch',$this->branch,true);
		$criteria->compare('divre_id',$this->divre_id);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kabupaten_id',$this->kabupaten_id);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('hotline',$this->hotline,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('long',$this->long,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Branch the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
