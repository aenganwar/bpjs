<?php

/**
 * This is the model class for table "faskes".
 *
 * The followings are the available columns in table 'faskes':
 * @property integer $id
 * @property integer $branch_id
 * @property string $nama
 * @property integer $faskes_class_id
 * @property integer $faskes_tipe_id
 * @property string $alamat
 * @property integer $kelurahan_id
 * @property string $phone
 * @property string $fax
 * @property string $hotline
 * @property string $lat
 * @property string $long
 */
class Faskes extends CActiveRecord
{
	public $provinsi;
	public $kabupaten;
	public $kecamatan;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'faskes';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('branch_id, nama, faskes_class_id, faskes_tipe_id', 'required'),
			array('branch_id, faskes_class_id, provinsi, kabupaten, kecamatan, faskes_tipe_id, kelurahan_id', 'numerical', 'integerOnly'=>true),
			array('nama', 'length', 'max'=>255),
			array('phone, fax, hotline, lat, long', 'length', 'max'=>50),
			array('alamat, keterangan, map_text', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, branch_id, nama, faskes_class_id, faskes_tipe_id, alamat, keterangan, map_text, kelurahan_id, provinsi, kabupaten, kecamatan, phone, fax, hotline, lat, long', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branch_id' => 'Branch',
			'nama' => 'Nama',
			'faskes_class_id' => 'Class Fasilitas Kesehatan',
			'faskes_tipe_id' => 'Tipe Fasilitas Kesehatan',
			'alamat' => 'Alamat',
			'keterangan' => 'Keterangan',
			'provinsi' => 'Provinsi',
			'kabupaten' => 'Kabupaten/Kota',
			'kecamatan' => 'Kecamatan',
			'kelurahan_id' => 'Kelurahan/Desa',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'hotline' => 'Hotline',
			'lat' => 'Lat',
			'long' => 'Long',
			'map_text' => 'Map Text',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('faskes_class_id',$this->faskes_class_id);
		$criteria->compare('faskes_tipe_id',$this->faskes_tipe_id);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('kelurahan_id',$this->kelurahan_id);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('hotline',$this->hotline,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('long',$this->long,true);
		$criteria->compare('map_text',$this->map_text,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Faskes the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
