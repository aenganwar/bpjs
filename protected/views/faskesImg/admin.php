<?php
$this->breadcrumbs=array(
	'Faskes Imgs'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List FaskesImg','url'=>array('index')),
array('label'=>'Create FaskesImg','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('faskes-img-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Faskes Imgs</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'faskes-img-grid',
'type'=>'bordered hover condensed striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		'faskes_id',
		'img',
		'keterangan',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
