<?php
$this->breadcrumbs=array(
	'Faskes Imgs'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List FaskesImg','url'=>array('index')),
array('label'=>'Manage FaskesImg','url'=>array('admin')),
);
?>

<h1>Create FaskesImg</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>