<?php
$this->breadcrumbs=array(
	'Faskes Imgs',
);

$this->menu=array(
array('label'=>'Create FaskesImg','url'=>array('create')),
array('label'=>'Manage FaskesImg','url'=>array('admin')),
);
?>

<h1>Faskes Imgs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
