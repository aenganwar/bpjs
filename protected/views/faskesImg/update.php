<?php
$this->breadcrumbs=array(
	'Faskes Imgs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List FaskesImg','url'=>array('index')),
	array('label'=>'Create FaskesImg','url'=>array('create')),
	array('label'=>'View FaskesImg','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage FaskesImg','url'=>array('admin')),
	);
	?>

	<h1>Update FaskesImg <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>