<?php
$this->breadcrumbs=array(
	'Faskes Imgs'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List FaskesImg','url'=>array('index')),
array('label'=>'Create FaskesImg','url'=>array('create')),
array('label'=>'Update FaskesImg','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete FaskesImg','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage FaskesImg','url'=>array('admin')),
);
?>

<h1>View FaskesImg #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'faskes_id',
		'img',
		'keterangan',
),
)); ?>
