<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'branch-coverage-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model,'branch_id',array('value'=>$branch_id));?>

	<?php echo $form->dropDownListRow($model,'provinsi', CHtml::listData(Provinsi::model()->findAll(), 'id', 'nama'),
				array('prompt'=>Yii::t('form','-Pilih Provinsi-'),
					'class'=>'chzn-select',
					 'ajax' => array(
						 'type'=>'POST',  
						 'url'=>CController::createUrl('kabupaten/getKabupaten'), 
						 'data'=>array('id'=>'js:$(this).val()','klas'=>'js:$("#'.CHtml::activeId($model, "provinsi_id").'").val()'),
						 'success'=>'function(data){
							 $("#'.CHtml::activeId($model, "kabupaten_id").'").html(data);
							 $("#'.CHtml::activeId($model, "kabupaten_id").'").trigger("liszt:updated");
						 }',
					 ),	
					 'style'=>'width:250px;',
				      ));?>

	<?php echo $form->dropDownListRow($model,'kabupaten_id', CHtml::listData(Kabupaten::model()->findAll(),'id', 'nama'), array('prompt'=>Yii::t('form','-Pilih Kabupaten-'),'class'=>'chzn-select span3')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<?php $this->widget( 'ext.EChosen.EChosen' ); ?>