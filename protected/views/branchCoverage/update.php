<?php
$this->breadcrumbs=array(
	'Branch Coverages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List BranchCoverage','url'=>array('index')),
	array('label'=>'Create BranchCoverage','url'=>array('create')),
	array('label'=>'View BranchCoverage','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage BranchCoverage','url'=>array('admin')),
	);
	?>

	<h1>Update BranchCoverage <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>