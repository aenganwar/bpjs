<?php
$this->breadcrumbs=array(
	'Branch Coverages'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List BranchCoverage','url'=>array('index')),
array('label'=>'Create BranchCoverage','url'=>array('create')),
array('label'=>'Update BranchCoverage','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete BranchCoverage','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage BranchCoverage','url'=>array('admin')),
);
?>

<h1>View BranchCoverage #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'branch_id',
		'kabupaten_id',
),
)); ?>
