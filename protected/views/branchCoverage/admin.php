<?php
$this->breadcrumbs=array(
	'Branch Coverages'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List BranchCoverage','url'=>array('index')),
array('label'=>'Create BranchCoverage','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('branch-coverage-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Branch Coverages</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'branch-coverage-grid',
'type'=>'bordered hover condensed striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		'branch_id',
		'kabupaten_id',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
