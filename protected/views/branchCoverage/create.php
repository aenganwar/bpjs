<?php
$this->breadcrumbs=array(
	'Branch Coverages'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List BranchCoverage','url'=>array('index')),
array('label'=>'Manage BranchCoverage','url'=>array('admin')),
);
?>

<h1>Create BranchCoverage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>