<?php
$this->breadcrumbs=array(
	'Branch Coverages',
);

$this->menu=array(
array('label'=>'Create BranchCoverage','url'=>array('create')),
array('label'=>'Manage BranchCoverage','url'=>array('admin')),
);
?>

<h1>Branch Coverages</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
