<?php
$this->breadcrumbs=array(
	'Faskes Tipes'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List FaskesTipe','url'=>array('index')),
array('label'=>'Create FaskesTipe','url'=>array('create')),
array('label'=>'Update FaskesTipe','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete FaskesTipe','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage FaskesTipe','url'=>array('admin')),
);
?>

<h1>View FaskesTipe #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'tipe_faskes',
		'kode',
		'jenis',
),
)); ?>
