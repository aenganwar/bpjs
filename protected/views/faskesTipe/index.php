<?php
$this->breadcrumbs=array(
	'Faskes Tipes',
);

$this->menu=array(
array('label'=>'Create FaskesTipe','url'=>array('create')),
array('label'=>'Manage FaskesTipe','url'=>array('admin')),
);
?>

<h1>Faskes Tipes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
