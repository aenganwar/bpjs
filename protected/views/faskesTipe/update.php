<?php
$this->breadcrumbs=array(
	'Faskes Tipes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List FaskesTipe','url'=>array('index')),
	array('label'=>'Create FaskesTipe','url'=>array('create')),
	array('label'=>'View FaskesTipe','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage FaskesTipe','url'=>array('admin')),
	);
	?>

	<h1>Update FaskesTipe <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>