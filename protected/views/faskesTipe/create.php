<?php
$this->breadcrumbs=array(
	'Faskes Tipes'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List FaskesTipe','url'=>array('index')),
array('label'=>'Manage FaskesTipe','url'=>array('admin')),
);
?>

<h1>Create FaskesTipe</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>