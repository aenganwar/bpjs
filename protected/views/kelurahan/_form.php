<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'kelurahan-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'provinsi', CHtml::listData(Provinsi::model()->findAll(), 'id', 'nama'),
				array('prompt'=>Yii::t('form','-Pilih Provinsi-'),
				      'class'=>'chzn-select',
					 'ajax' => array(
						 'type'=>'POST',  
						 'url'=>CController::createUrl('kabupaten/getKabupaten'), 
						 'data'=>array('id'=>'js:$(this).val()','klas'=>'js:$("#'.CHtml::activeId($model, "provinsi").'").val()'),
						 'success'=>'function(data){
							 $("#'.CHtml::activeId($model, "kabupaten").'").html(data);
							 $("#'.CHtml::activeId($model, "kabupaten").'").trigger("liszt:updated");
						 }',
					 ),	
					 'style'=>'width:250px;',
				      ));?>
	
	
	<?php echo $form->dropDownListRow($model,'kabupaten', CHtml::listData(Kabupaten::model()->findAll(), 'id', 'nama'),
			array('prompt'=>Yii::t('form','-Pilih Kabupaten-'),
				  'class'=>'chzn-select',
				 'ajax' => array(
					 'type'=>'POST',  
					 'url'=>CController::createUrl('kecamatan/getKecamatan'), 
					 'data'=>array('id'=>'js:$(this).val()','klas'=>'js:$("#'.CHtml::activeId($model, "kabupaten").'").val()'),
					 'success'=>'function(data){
						 $("#'.CHtml::activeId($model, "kecamatan_id").'").html(data);
						 $("#'.CHtml::activeId($model, "kecamatan_id").'").trigger("liszt:updated");
					 }',
				 ),	
				 'style'=>'width:250px;',
				  ));?>

	<?php echo $form->dropDownListRow($model,'kecamatan_id', CHtml::listData(Kecamatan::model()->findAll(array('order'=>'nama')),'id', 'nama'), array('prompt'=>Yii::t('form','-Pilih Kecamatan-'),'class'=>'chzn-select')); ?>
		
	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<?php $this->widget( 'ext.EChosen.EChosen' ); ?>