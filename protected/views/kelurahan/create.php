<?php
$this->breadcrumbs=array(
	'Kelurahans'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Kelurahan','url'=>array('index')),
array('label'=>'Manage Kelurahan','url'=>array('admin')),
);
?>

<h1>Create Kelurahan</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>