<?php
$this->breadcrumbs=array(
	'Kelurahans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Kelurahan','url'=>array('index')),
array('label'=>'Create Kelurahan','url'=>array('create')),
array('label'=>'Update Kelurahan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Kelurahan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Kelurahan','url'=>array('admin')),
);
?>

<h1>View Kelurahan #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'kecamatan_id',
		'nama',
		'keterangan',
		'create_time',
		'update_time',
		'create_user_id',
		'update_user_id',
),
)); ?>
