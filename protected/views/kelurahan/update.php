<?php
$this->breadcrumbs=array(
	'Kelurahans'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Kelurahan','url'=>array('index')),
	array('label'=>'Create Kelurahan','url'=>array('create')),
	array('label'=>'View Kelurahan','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Kelurahan','url'=>array('admin')),
	);
	?>

	<h1>Update Kelurahan <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>