<?php
$this->breadcrumbs=array(
	'Kelurahans'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Kelurahan','url'=>array('index')),
array('label'=>'Create Kelurahan','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kelurahan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Kelurahan</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'kelurahan-grid',
'type'=>'bordered hover striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		'kecamatan_id',
		'nama',
		'keterangan',
		'create_time',
		'update_time',
		/*
		'create_user_id',
		'update_user_id',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
