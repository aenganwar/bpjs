<?php
$this->breadcrumbs=array(
	'Faskes'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Faskes','url'=>array('index')),
array('label'=>'Create Faskes','url'=>array('create')),
array('label'=>'Update Faskes','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Faskes','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Faskes','url'=>array('admin')),
);
?>

<h1>View Faskes #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		//'id',
		'branch_id',
		'nama',
		'faskes_class_id',
		'faskes_tipe_id',
                'keterangan',
		'alamat',
		'kelurahan_id',
		'phone',
		'fax',
		'hotline',
		'lat',
		'long',
                'map_text',
),
)); ?>

<legend>Gambar Fasilitas Kesehatan</legend>

<div class="row-fluid">
        <div class="span12" style="background-color:#F5F5F5; border-radius:3px">
        <?php foreach(FaskesImg::model()->findAllByAttributes(array('faskes_id'=>$model->id)) as $key=>$gambar){;?>
            <div class="btn-group" style="margin-bottom:10px; margin:5px;">    
                <a class="dropdown-toggle"  id="yw<?php echo $key;?>" data-toggle="dropdown" href="#" rel="tooltip" title="<?php echo $gambar->keterangan;?>" >
                    <img style="height:130px; padding:3px" class="img-polaroid" src="<?php echo Yii::app()->request->baseUrl.'/images/faskes/'.$gambar->img;?>">
                </a>
                
                <ul class="dropdown-menu" id="yw<?php echo $key+1;?>">
                    <li><?php echo CHtml::link('<i class="icon-trash"></i> Hapus',array('faskesImg/hapus','id'=>$gambar->id),array('confirm' => 'Are you sure, you want to delete this image?'));?></li>
                </ul> 	
            </div>
        <?php }?>
        </div>
</div>

<div class="form-actions" style="margin-top:0px">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
                                    'buttonType'=>'link',
                                    'type'=>'success',
                                    'icon'=>'upload white',
                                    'size'=>'mini',
                                    'label'=>'Add Image',
                                    'htmlOptions'=>array('class'=>'pull-right',
                                                        'data-toggle'=>'modal',
                                                        'style'=>'margin-right:10px',
                                                        'data-target'=>'#addImage',
                                                        ),
    )); ?>
</div>

<!-- Show Modal Add Foto-->
<?php $this->beginWidget('bootstrap.widgets.TbModal', array('id'=>'addImage')); ?>
<div class="modal-header">
    <a class="close" data-dismiss="modal">&times;</a>
    <h4>Tambahkan Gambar</h4>
    <p>Tambahkan Gambar Untuk Fasilitas Kesehatan.</p>
</div>

<div class="modal-body">
    <?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	    'id'=>'faskes-img-form',
	    'enableAjaxValidation'=>false,
            'htmlOptions' =>array('enctype'=>"multipart/form-data"),
            'action'=>array('faskesImg/create'),
    )); ?>

	<?php echo $form->hiddenField($faskesImg,'faskes_id',array('class'=>'span5', 'value'=>$model->id)); ?>

	<?php echo $form->filefieldRow($faskesImg,'img'); ?><br><br>

	<?php echo $form->textAreaRow($faskesImg,'keterangan',array('rows'=>4, 'cols'=>50, 'class'=>'span8')); ?>

</div>

<div class="modal-footer">
    
    <?php $this->widget('bootstrap.widgets.TbButton', array(
							'label'=>'Cancel',
							'icon'=>'repeat',
							'url'=>'#',
							'htmlOptions'=>array('data-dismiss'=>'modal'),
						    )); ?>
    
    <?php $this->widget('bootstrap.widgets.TbButton',array('buttonType'=>'submit',
							  'label'=>'Save',
							  'icon'=>'ok white',
							  'type'=>'success',
							  )); ?>
</div>
<?php $this->endWidget(); ?>
<?php $this->endWidget(); ?>
