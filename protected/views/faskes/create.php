<?php
$this->breadcrumbs=array(
	'Faskes'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Faskes','url'=>array('index')),
array('label'=>'Manage Faskes','url'=>array('admin')),
);
?>

<h1>Create Faskes</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>