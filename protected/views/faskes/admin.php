<?php
$this->breadcrumbs=array(
	'Faskes'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Faskes','url'=>array('index')),
array('label'=>'Create Faskes','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('faskes-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Daftar Fasilitas Kesehatan</h1>
<?php /*
<p>
	You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>
		&lt;&gt;</b>
	or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
*/?>
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn')); ?>
<div class="search-form" style="display:none">
	<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'faskes-grid',
'type'=>'bordered hover condensed striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		),
		array(
		      'name'=>'branch_id',
		      'value'=>'Branch::model()->findByPk($data->branch_id)->branch',
		),
		'nama',
		'faskes_class_id',
		//'faskes_tipe_id',
		array(
		      'name'=>'faskes_tipe_id',
		      'value'=>'FaskesTipe::model()->findByPk($data->faskes_tipe_id)->jenis',
		),
		'alamat',
		/*
		'kelurahan_id',
		'phone',
		'fax',
		'hotline',
		'lat',
		'long',
		*/
array(
	'header'=>'Options',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
