<?php
$this->breadcrumbs=array(
	'Faskes'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Faskes','url'=>array('index')),
	array('label'=>'Create Faskes','url'=>array('create')),
	array('label'=>'View Faskes','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Faskes','url'=>array('admin')),
	);
	?>

	<h1>Update Faskes <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>