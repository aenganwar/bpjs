<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'editform',
	'enableAjaxValidation'=>false,
	//'type'=>'horizontal',
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'branch_id', CHtml::listData(Branch::model()->findAll(), 'id', 'branch'), array('prompt'=>Yii::t('form','-Pilih Branch-'),'class'=>'chzn-select')); ?>

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'faskes_tipe_id', CHtml::listData(FaskesTipe::model()->findAll(), 'id', 'kode'), array('prompt'=>Yii::t('form','-Pilih Tipe Fasilitas Kesehatan-'),'class'=>'chzn-select')); ?>

	<?php echo $form->dropDownListRow($model,'faskes_class_id', CHtml::listData(FaskesClass::model()->findAll(), 'id', 'class'), array('prompt'=>Yii::t('form','-Pilih Class-'),'class'=>'chzn-select')); ?>

	<?php echo $form->ckEditorRow($model,'keterangan',array('options' => array('fullpage' => 'js:true',
										  'width' => '740',
										  'height'=>'300',
										  'resize_maxWidth' => '440',
										  'resize_minWidth' => '220'
										  ))); ?>
<legend>Lokasi :</legend>
	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>3, 'cols'=>50, 'class'=>'span5')); ?>
	
	<?php echo $form->dropDownListRow($model,'provinsi', CHtml::listData(Provinsi::model()->findAll(), 'id', 'nama'),
				array('prompt'=>Yii::t('form','-Pilih Provinsi-'),
				      'class'=>'chzn-select',
					 'ajax' => array(
						 'type'=>'POST',  
						 'url'=>CController::createUrl('kabupaten/getKabupaten'), 
						 'data'=>array('id'=>'js:$(this).val()','klas'=>'js:$("#'.CHtml::activeId($model, "provinsi").'").val()'),
						 'success'=>'function(data){
							 $("#'.CHtml::activeId($model, "kabupaten").'").html(data);
							 $("#'.CHtml::activeId($model, "kabupaten").'").trigger("liszt:updated");
						 }',
					 ),	
					 'style'=>'width:250px;',
				      ));?>
	
	<?php echo $form->dropDownListRow($model,'kabupaten', CHtml::listData(Kabupaten::model()->findAll(), 'id', 'nama'),
				array('prompt'=>Yii::t('form','-Pilih Kabupaten-'),
				      'class'=>'chzn-select',
					 'ajax' => array(
						 'type'=>'POST',  
						 'url'=>CController::createUrl('kecamatan/getKecamatan'), 
						 'data'=>array('id'=>'js:$(this).val()','klas'=>'js:$("#'.CHtml::activeId($model, "kabupaten").'").val()'),
						 'success'=>'function(data){
							 $("#'.CHtml::activeId($model, "kecamatan").'").html(data);
							 $("#'.CHtml::activeId($model, "kecamatan").'").trigger("liszt:updated");
						 }',
					 ),	
					 'style'=>'width:250px;',
				      ));?>
	
	<?php echo $form->dropDownListRow($model,'kecamatan', CHtml::listData(Kecamatan::model()->findAll(), 'id', 'nama'),
				array('prompt'=>Yii::t('form','-Pilih Kecamatan-'),
				      'class'=>'chzn-select',
					'ajax' => array(
						 'type'=>'POST',  
						 'url'=>CController::createUrl('kelurahan/getKelurahan'), 
						 'data'=>array('id'=>'js:$(this).val()','klas'=>'js:$("#'.CHtml::activeId($model, "kecamatan").'").val()'),
						 'success'=>'function(data){
							 $("#'.CHtml::activeId($model, "kelurahan_id").'").html(data);
							 $("#'.CHtml::activeId($model, "kelurahan_id").'").trigger("liszt:updated");
						 }',
					 ),	
					 'style'=>'width:250px;',
				      ));?>
	
		

	<?php echo $form->dropDownListRow($model,'kelurahan_id', CHtml::listData(Kelurahan::model()->findAll(), 'id', 'nama'), array('prompt'=>Yii::t('form','-Pilih Kelurahan-'),'class'=>'chzn-select')); ?>
		
	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'fax',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'hotline',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->hiddenfield($model,'id',array('class'=>'span3','maxlength'=>50)); ?>
	
	<?php echo $form->hiddenfield($model,'lat',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->hiddenfield($model,'long',array('class'=>'span3','maxlength'=>50)); ?>
	
	<div class="item block_gispoint nullf" >
		<?php echo $form->textAreaRow($model,'map_text',array('rows'=>6, 'cols'=>50, 'class'=>'span6')); ?>
		
		<?php if($model->isNewRecord){ ?>
		<span id="map_text" class="indmap" onclick="window.open('../site/showMap', 'wgispointMap', 'width=850,height=650,scrollbars=1,toolbar=0,menubar=0,location=0,status=0,resizable=0');return false;" rel="tooltip" title="Indicate on the map"></span>
		<?php }else{ ?>
		<span id="map_text" class="indmap" onclick="window.open('../../site/showMap?faskes_id='+$('#Faskes_id').val(), 'wgispointMap', 'width=850,height=650,scrollbars=1,toolbar=0,menubar=0,location=0,status=0,resizable=0');return false;" rel="tooltip" title="Indicate on the map"></span>
		<?php } ?>
	</div>
	
<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<?php $this->widget( 'ext.EChosen.EChosen' ); ?>
