<?php
$this->breadcrumbs=array(
	'Faskes',
);

$this->menu=array(
array('label'=>'Create Faskes','url'=>array('create')),
array('label'=>'Manage Faskes','url'=>array('admin')),
);
?>

<h1>Faskes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
