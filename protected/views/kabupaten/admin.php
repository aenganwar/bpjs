<?php
$this->breadcrumbs=array(
	'Kabupatens'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Kabupaten','url'=>array('index')),
array('label'=>'Create Kabupaten','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kabupaten-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Kabupaten</h1>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'kabupaten-grid',
'type'=>'bordered hover condensed striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		'provinsi_id',
		'nama',
		'jenis',
		'keterangan',
		'create_time',
		/*
		'update_time',
		'create_user_id',
		'update_user_id',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
