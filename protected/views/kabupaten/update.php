<?php
$this->breadcrumbs=array(
	'Kabupatens'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Kabupaten','url'=>array('index')),
	array('label'=>'Create Kabupaten','url'=>array('create')),
	array('label'=>'View Kabupaten','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Kabupaten','url'=>array('admin')),
	);
	?>

	<h1>Update Kabupaten <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>