<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'kabupaten-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'provinsi_id', CHtml::listData(Provinsi::model()->findAll(array('order'=>'nama')), 'id', 'nama'), array('prompt'=>Yii::t('form','-Pilih Provinsi-'),'class'=>'chzn-select')); ?>

	<?php echo $form->dropDownListRow($model,'jenis',array("0"=>"KABUPATEN", "1"=>"KOTA"), array('class'=>'chzn-select')); ?>
	
	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<?php $this->widget( 'ext.EChosen.EChosen' ); ?>