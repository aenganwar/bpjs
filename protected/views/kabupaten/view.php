<?php
$this->breadcrumbs=array(
	'Kabupatens'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Kabupaten','url'=>array('index')),
array('label'=>'Create Kabupaten','url'=>array('create')),
array('label'=>'Update Kabupaten','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Kabupaten','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Kabupaten','url'=>array('admin')),
);
?>

<h1>View Kabupaten #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'provinsi_id',
		'nama',
		'jenis',
		'keterangan',
		'create_time',
		'update_time',
		'create_user_id',
		'update_user_id',
),
)); ?>
