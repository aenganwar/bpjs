<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>
<div id="map">
<?php 
//
// ext is your protected.extensions folder
// gmaps means the subfolder name under your protected.extensions folder
//  
Yii::import('ext.gmap.*');
 
$gMap = new EGMap();
$gMap->zoom = 5;
$mapTypeControlOptions = array(
  'position'=> EGMapControlPosition::LEFT_BOTTOM,
  'style'=>EGMap::MAPTYPECONTROL_STYLE_DROPDOWN_MENU
);
 
$gMap->mapTypeControlOptions= $mapTypeControlOptions;
 
$gMap->setCenter(-0.729824, 117.434456);
 
foreach(Faskes::model()->findAll() as $faskes):
$objek = '<div id="showData-map">
    <h4 class="judul">'.CHtml::link($faskes->nama, array('/faskes/view', 'id'=>$faskes->id)).'</h4>
    <p class="content-kategori"><i class="icon-bookmark icon-white"></i> '.FaskesTipe::model()->findByPk($faskes->faskes_tipe_id)->jenis.' ('.FaskesTipe::model()->findByPk($faskes->faskes_tipe_id)->kode.')</p>
      <div class="row-fluid">
           <div class="span4">
         <img id="thumb-map" src="'.Yii::app()->baseUrl.'/images/faskes/no-img.png" />
           </div>
           <div class="span8">
         <p class="content-objek">'.substr(strip_tags($faskes->alamat),0,100).' ...</p>
           </div>
       </div>
   </div>
      ';
// Create GMapInfoWindows
$info_window = new EGMapInfoWindow($objek);
$icon = new EGMapMarkerImage(Yii::app()->baseUrl.'/images/map_icons/justice.png');
$icon->setSize(30, 35);
$icon->setAnchor(16, 16.5);
$icon->setOrigin(0, 0);

// Create marker
$marker = new EGMapMarker($faskes->lat, $faskes->long, array('title' => $faskes->nama, 'icon'=>$icon));
$marker->addHtmlInfoWindow($info_window);
$gMap->addMarker($marker);

// enabling marker clusterer just for fun
// to view it zoom-out the map
$gMap->enableMarkerClusterer(new EGMapMarkerClusterer());
endforeach;
 
$gMap->renderMap();

?>
</div>
