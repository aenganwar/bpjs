<?php
$this->breadcrumbs=array(
	'Kecamatans'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List Kecamatan','url'=>array('index')),
array('label'=>'Create Kecamatan','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('kecamatan-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Kecamatans</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'kecamatan-grid',
'type'=>'bordered striped hover'
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		'kabupaten_id',
		'nama',
		'keterangan',
		'create_time',
		'update_time',
		/*
		'create_user_id',
		'update_user_id',
		*/
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
