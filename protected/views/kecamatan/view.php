<?php
$this->breadcrumbs=array(
	'Kecamatans'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Kecamatan','url'=>array('index')),
array('label'=>'Create Kecamatan','url'=>array('create')),
array('label'=>'Update Kecamatan','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Kecamatan','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Kecamatan','url'=>array('admin')),
);
?>

<h1>View Kecamatan #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'kabupaten_id',
		'nama',
		'keterangan',
		'create_time',
		'update_time',
		'create_user_id',
		'update_user_id',
),
)); ?>
