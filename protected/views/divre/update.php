<?php
$this->breadcrumbs=array(
	'Divres'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List Divre','url'=>array('index')),
	array('label'=>'Create Divre','url'=>array('create')),
	array('label'=>'View Divre','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage Divre','url'=>array('admin')),
	);
	?>

	<h1>Update Divre <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>