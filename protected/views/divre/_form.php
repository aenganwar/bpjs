<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'divre-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'regional',array('class'=>'span2','maxlength'=>50)); ?>

	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->dropDownListRow($model,'kabupaten_id', CHtml::listData(Kabupaten::model()->findAll(),'id', 'nama'), array('prompt'=>Yii::t('form','-Pilih Kabupaten-'),'class'=>'chzn-select span3')); ?>

	<?php echo $form->textFieldRow($model,'zip',array('class'=>'span2','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'fax',array('class'=>'span2','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'hotline',array('class'=>'span3','maxlength'=>50)); ?>

	<?php echo $form->textAreaRow($model,'keterangan',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'lat',array('class'=>'span3','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'long',array('class'=>'span3','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<?php $this->widget( 'ext.EChosen.EChosen' ); ?>