<?php
$this->breadcrumbs=array(
	'Divres'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Divre','url'=>array('index')),
array('label'=>'Manage Divre','url'=>array('admin')),
);
?>

<h1>Create Divre</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>