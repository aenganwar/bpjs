<?php
$this->breadcrumbs=array(
	'Tenaga Medises'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TenagaMedis','url'=>array('index')),
array('label'=>'Manage TenagaMedis','url'=>array('admin')),
);
?>

<h1>Create TenagaMedis</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>