<?php
$this->breadcrumbs=array(
	'Tenaga Medises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List TenagaMedis','url'=>array('index')),
	array('label'=>'Create TenagaMedis','url'=>array('create')),
	array('label'=>'View TenagaMedis','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage TenagaMedis','url'=>array('admin')),
	);
	?>

	<h1>Update TenagaMedis <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>