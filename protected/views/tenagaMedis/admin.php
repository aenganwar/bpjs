<?php
$this->breadcrumbs=array(
	'Tenaga Medises'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List TenagaMedis','url'=>array('index')),
array('label'=>'Create TenagaMedis','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('tenaga-medis-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Kelola Tenaga Medise</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'tenaga-medis-grid',
'type'=>'bordered striped hover',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
		'nama',
		'jenis_tenaga_medis_id',
		'alamat',
		'kabupaten_id',
array(
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
