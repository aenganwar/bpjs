<?php
$this->breadcrumbs=array(
	'Tenaga Medises'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List TenagaMedis','url'=>array('index')),
array('label'=>'Create TenagaMedis','url'=>array('create')),
array('label'=>'Update TenagaMedis','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete TenagaMedis','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage TenagaMedis','url'=>array('admin')),
);
?>

<h1>View TenagaMedis #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'nama',
		'jenis_tenaga_medis_id',
		'alamat',
		'kabupaten_id',
),
)); ?>
