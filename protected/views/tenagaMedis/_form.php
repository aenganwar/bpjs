<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'tenaga-medis-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
	<?php echo $form->dropDownListRow($model,'faskes_id', CHtml::listData(Faskes::model()->findAll(), 'id', 'nama'), array('prompt'=>Yii::t('form','-Pilih Fasilitas Kesehatan-'),'class'=>'chzn-select')); ?>
	

	<?php echo $form->textFieldRow($model,'nama',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->dropDownListRow($model,'jenis_tenaga_medis_id', CHtml::listData(JenisTenagaMedis::model()->findAll(), 'id', 'jenis'), array('prompt'=>Yii::t('form','-Pilih Jenis Tenaga Medis-'),'class'=>'chzn-select')); ?>
	
	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->textFieldRow($model,'kabupaten_id',array('class'=>'span5')); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<?php $this->widget( 'ext.EChosen.EChosen' ); ?>