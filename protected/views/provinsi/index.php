<?php
$this->breadcrumbs=array(
	'Provinsis',
);

$this->menu=array(
array('label'=>'Create Provinsi','url'=>array('create')),
array('label'=>'Manage Provinsi','url'=>array('admin')),
);
?>

<h1>Provinsis</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
