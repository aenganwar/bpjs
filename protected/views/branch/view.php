<?php
$this->breadcrumbs=array(
	'Branches'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List Branch','url'=>array('index')),
array('label'=>'Create Branch','url'=>array('create')),
array('label'=>'Update Branch','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete Branch','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage Branch','url'=>array('admin')),
);
?>

<h1>View Branch #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'branch',
		'divre_id',
		'alamat',
		'kabupaten_id',
		'zip',
		'phone',
		'fax',
		'hotline',
		'lat',
		'long',
),
)); ?>

<legend>Coverage Branch</legend>
<p>Wilayah Branch <?php echo $model->branch;?> meliputi coverage.</p>
<?php
$branchCoverage = new CActiveDataProvider('BranchCoverage', array(
                                                            'criteria'=>array(
                                                            'condition'=>'branch_id=:ID',
                                                            'params'=>array(':ID'=>$model->id),
                                                            'order'=>'branch_id ASC',
                                                                        ),
                                                        ));
?>

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'branch-coverage-grid',
	'dataProvider'=>$branchCoverage,
	//'filter'=>$model,
	'columns'=>array(
		array(
		    'header'=>'No',
		    'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
                    'htmlOptions'=>array(
					'style'=>'text-align:center',
                                        'width'=>'20px',
					),
                ),
		array(
		      'name'=>'kabupaten_id',
		      'value'=>'$data->kabupaten->nama',
		      'filter'=>'',
		),
		array(
			'header'=>'Options',
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update}{delete}',
			'htmlOptions'=>array(
					     'style'=>'width: 60px',
					     'style'=>'text-align:center',
					     ),
			'buttons' => array(
					    'update' => array(
					    'url'=>'Yii::app()->createUrl("branchCoverage/update", array("id"=>$data->id))', 
					    'click' => "function(e){
						    e.preventDefault();
						    $('#update-dialog').children(':eq(0)').empty(); // Stop auto POST
						    updateDialog($(this).attr('href'));
						    $('#update-dialog').dialog({title:'Update Data Branch Coverage'}).dialog('open');
					    }",
				    ),
				    'delete' => array(
					    'url'=>'Yii::app()->createUrl("branchCoverage/delete", array("id"=>$data->id))', 
				    ),
			    ),
			'htmlOptions'=>array(
					    'style'=>'text-align:center',
					    ),
		),
	),
)); ?>

<?php echo CHtml::button(Yii::t('view','Tambah Coverage'),array(
					'onclick'=>'addArchive();
					$("#update-dialog").dialog({title:"Tambah Branch Coverage"}).dialog("open");
					return false;',
					)); ?>



<script type="text/javascript">

function addArchive()
{
    <?php 
	echo CHtml::ajax(array(
            'url'=>array('branchCoverage/create','branch_id'=>$model->id),
            'data'=> "js:$(this).serialize()",
            'type'=>'post',
            'dataType'=>'json',
            'success'=>"function(data)
			{
				if (data.status == 'failure')
				{
					$('#update-dialog div.update-dialog-content').html(data.content);
					// Here is the trick: on submit-> once again this function!
					$('#update-dialog div.update-dialog-content form').submit(addArchive);
				}
				else
				{
					$.fn.yiiGridView.update('branch-coverage-grid');
					$('#update-dialog div.update-dialog-content').html(data.content);
					setTimeout(\"$('#update-dialog').dialog('close') \",1000);
					
				} 
			} ",
            ))
	?>;
    return false; 
}
</script>

<?php $this->beginWidget( 'zii.widgets.jui.CJuiDialog', array(
	'id' => 'update-dialog',
	'options' => array(
		'title' => 'Dialog',
		'autoOpen' => false,
		'modal' => true,
		'width' => 500,
		'height' => 350,
		'resizable' => true,
		'position' =>'center',
	),
)); 
echo '<div class="update-dialog-content"></div>';
$this->endWidget(); ?>

<?php
  $updateJS = CHtml::ajax( array(
  'url' => "js:url",
  'data' => "js:form.serialize() + action",
  'type' => 'post',
  'dataType' => 'json',
  'success' => "function( data )
  {
    if( data.status == 'failure' )
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      $( '#update-dialog div.update-dialog-content form input[type=submit]' )
        .die() // Stop from re-binding event handlers
        .live( 'click', function( e ){ // Send clicked button value
          e.preventDefault();
          updateDialog( false, $( this ).attr( 'name' ) );
      });
    }
    else
    {
      $( '#update-dialog div.update-dialog-content' ).html( data.content );
      if( data.status == 'success' ) // Update all grid views on success
      {
        $( 'div.grid-view' ).each( function(){ // Change the selector if you use different class or element
          $.fn.yiiGridView.update( $( this ).attr( 'id' ) );
        });
      }
      setTimeout( \"$( '#update-dialog' ).dialog( 'close' ).children( ':eq(0)' ).empty();\", 1000 );
    }
  }"
)); ?>

<?php
Yii::app()->clientScript->registerScript( 'updateDialog', "
function updateDialog( url, act )
{
  var action = '';
  var form = $( '#update-dialog div.update-dialog-content form' );
  if( url == false )
  {
    action = '&action=' + act;
    url = form.attr( 'action' );
  }
  {$updateJS}
}" ); ?>

<?php
Yii::app()->clientScript->registerScript( 'updateDialogCreate', "
jQuery( function($){
    $( 'a.update-dialog-create' ).bind( 'click', function( e ){
      e.preventDefault();
      $( '#update-dialog' ).children( ':eq(0)' ).empty();
      updateDialog( $( this ).attr( 'href' ) );
      $( '#update-dialog' )
        .dialog( { title: 'Create' } )
        .dialog( 'open' );
    });
});
" );?>
