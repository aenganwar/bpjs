<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'branch-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->dropDownListRow($model,'divre_id',CHtml::listData(Divre::model()->findAll(), 'id', 'regional'),  array('prompt'=>Yii::t('form','-Pilih Regional-'),'class'=>'chzn-select')); ?>
	
	<?php echo $form->textFieldRow($model,'branch',array('class'=>'span5','maxlength'=>255)); ?>

<legend>Office :</legend>
	<?php echo $form->textAreaRow($model,'alamat',array('rows'=>6, 'cols'=>50, 'class'=>'span8')); ?>

	<?php echo $form->dropDownListRow($model,'kabupaten_id', CHtml::listData(Kabupaten::model()->findAll(), 'id', 'nama'), array('prompt'=>Yii::t('form','-Pilih Kabupaten/Kota-'),'class'=>'chzn-select')); ?>

	<?php echo $form->textFieldRow($model,'zip',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'phone',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'fax',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'hotline',array('class'=>'span5','maxlength'=>50)); ?>

	<?php echo $form->textFieldRow($model,'lat',array('class'=>'span5','maxlength'=>255)); ?>

	<?php echo $form->textFieldRow($model,'long',array('class'=>'span5','maxlength'=>255)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<?php $this->widget( 'ext.EChosen.EChosen' ); ?>
