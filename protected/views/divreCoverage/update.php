<?php
$this->breadcrumbs=array(
	'Divre Coverages'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List DivreCoverage','url'=>array('index')),
	array('label'=>'Create DivreCoverage','url'=>array('create')),
	array('label'=>'View DivreCoverage','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage DivreCoverage','url'=>array('admin')),
	);
	?>

	<h1>Update DivreCoverage <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>