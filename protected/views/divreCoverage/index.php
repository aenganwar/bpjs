<?php
$this->breadcrumbs=array(
	'Divre Coverages',
);

$this->menu=array(
array('label'=>'Create DivreCoverage','url'=>array('create')),
array('label'=>'Manage DivreCoverage','url'=>array('admin')),
);
?>

<h1>Divre Coverages</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
'dataProvider'=>$dataProvider,
'itemView'=>'_view',
)); ?>
