<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'divre-coverage-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

	<?php echo $form->hiddenField($model,'divre_id', array('value'=>$divre_id));?>
	
	<?php echo $form->dropDownList($model,'provinsi', CHtml::listData(Provinsi::model()->findAll(), 'id', 'nama'),
				array('prompt'=>Yii::t('form','-Pilih Provinsi-'),
					'class'=>'chzn-select',
					 'ajax' => array(
						 'type'=>'POST',  
						 'url'=>CController::createUrl('kabupaten/getKabupaten'), 
						 'data'=>array('id'=>'js:$(this).val()','klas'=>'js:$("#'.CHtml::activeId($model, "provinsi_id").'").val()'),
						 'success'=>'function(data){
							 $("#'.CHtml::activeId($model, "kabupaten_id").'").html(data);
							 $("#'.CHtml::activeId($model, "kabupaten_id").'").trigger("liszt:updated");
						 }',
					 ),	
					 'style'=>'width:250px;',
				      ));?>
				      
	<?php echo $form->dropDownList($model,'kabupaten_id', CHtml::listData(Kabupaten::model()->findAll(), 'id', 'nama')array('prompt'=>Yii::t('form','-Pilih Kabupatenm-'), 'class'=>'chzn-select')));?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
</div>

<?php $this->endWidget(); ?>
<?php $this->widget( 'ext.EChosen.EChosen' ); ?>
