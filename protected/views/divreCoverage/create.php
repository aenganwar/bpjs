<?php
$this->breadcrumbs=array(
	'Divre Coverages'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List DivreCoverage','url'=>array('index')),
array('label'=>'Manage DivreCoverage','url'=>array('admin')),
);
?>

<h1>Create DivreCoverage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>