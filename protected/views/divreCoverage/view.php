<?php
$this->breadcrumbs=array(
	'Divre Coverages'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List DivreCoverage','url'=>array('index')),
array('label'=>'Create DivreCoverage','url'=>array('create')),
array('label'=>'Update DivreCoverage','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete DivreCoverage','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage DivreCoverage','url'=>array('admin')),
);
?>

<h1>View DivreCoverage #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'divre_id',
		'provinsi_id',
),
)); ?>
