<?php
$this->breadcrumbs=array(
	'Jenis Tenaga Medises'=>array('index'),
	'Manage',
);

$this->menu=array(
array('label'=>'List JenisTenagaMedis','url'=>array('index')),
array('label'=>'Create JenisTenagaMedis','url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$.fn.yiiGridView.update('jenis-tenaga-medis-grid', {
data: $(this).serialize()
});
return false;
});
");
?>

<h1>Manage Jenis Tenaga Medises</h1>


<?php $this->widget('bootstrap.widgets.TbGridView',array(
'id'=>'jenis-tenaga-medis-grid',
'type'=>'bordered hover condensed striped',
'dataProvider'=>$model->search(),
'filter'=>$model,
'columns'=>array(
		//'id',
		array(
			'header'=>'No',
			'value'=>'$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize+$row+1',
			'htmlOptions'=>array(
				'style'=>'text-align:center',
				'width'=>'30px',
		      ),
                ),
		'jenis',
array(
    'header'=>'Options',
'class'=>'bootstrap.widgets.TbButtonColumn',
),
),
)); ?>
