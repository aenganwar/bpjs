<?php
$this->breadcrumbs=array(
	'Jenis Tenaga Medises'=>array('index'),
	$model->id,
);

$this->menu=array(
array('label'=>'List JenisTenagaMedis','url'=>array('index')),
array('label'=>'Create JenisTenagaMedis','url'=>array('create')),
array('label'=>'Update JenisTenagaMedis','url'=>array('update','id'=>$model->id)),
array('label'=>'Delete JenisTenagaMedis','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
array('label'=>'Manage JenisTenagaMedis','url'=>array('admin')),
);
?>

<h1>View JenisTenagaMedis #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
'data'=>$model,
'attributes'=>array(
		'id',
		'jenis',
),
)); ?>
