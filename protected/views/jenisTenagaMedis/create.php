<?php
$this->breadcrumbs=array(
	'Jenis Tenaga Medises'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List JenisTenagaMedis','url'=>array('index')),
array('label'=>'Manage JenisTenagaMedis','url'=>array('admin')),
);
?>

<h1>Create JenisTenagaMedis</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>