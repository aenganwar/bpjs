<?php
$this->breadcrumbs=array(
	'Jenis Tenaga Medises'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

	$this->menu=array(
	array('label'=>'List JenisTenagaMedis','url'=>array('index')),
	array('label'=>'Create JenisTenagaMedis','url'=>array('create')),
	array('label'=>'View JenisTenagaMedis','url'=>array('view','id'=>$model->id)),
	array('label'=>'Manage JenisTenagaMedis','url'=>array('admin')),
	);
	?>

	<h1>Update JenisTenagaMedis <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>